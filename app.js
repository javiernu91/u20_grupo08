const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { API_VERSION } = require('./constants');

const app = express();

// Importación de las rutas
//controller autenticacion - routes autenticacion y por ultimo al app.js
const autenticacionRutas = require('./router/autenticacion');
const usuariosRoutes = require('./router/usuarios');
//const menuRoutes = require("./routes/menu");
const productoRoutes = require('./router/producto');
const ventaRoutes = require('./router/venta');
//const newsletterRoutes = require("./routes/newsletter");

// Configure Body Parser para enviar contenido del body json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Configure static folder uploads. Con esta linea me deja acceder en la web
//a todas las imagenes que se encuentra en upload : http://localhost:3900/+ la el nombre de la imagen

//app.use(express.static('uploads'));
app.use(express.json());
//Configure Header HTTP - CORS
app.use(cors());

var whitelist = ['http//localhost:3900/api/v1', 'http//localhost:4000/api/v1' ]
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = {Origin: true}
  } else {
    corsOptions = {Origin: false}
  }
  callback(null, corsOptions)
}

// Configuración de las rutas
app.use(`/api/${API_VERSION}`, cors(corsOptionsDelegate), autenticacionRutas);
app.use(`/api/${API_VERSION}`, cors(corsOptionsDelegate),usuariosRoutes);
//app.use(`/api/${API_VERSION}`, menuRoutes);
app.use(`/api/${API_VERSION}`, cors(corsOptionsDelegate), productoRoutes);
app.use(`/api/${API_VERSION}`, cors(corsOptionsDelegate),ventaRoutes);
//app.use(`/api/${API_VERSION}`, newsletterRoutes);

module.exports = app;
