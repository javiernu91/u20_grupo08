import Inicio from "./components/inicio"
import Login from "./components/login"
import logo from "./images/logo.png"
import carshop from "./images/car.png"
import Registro from "./components/register"
import Carrito from "./components/carrito"
import './App.css';
import { Link } from "react-router-dom"
import {Browser,Routes,Route, BrowserRouter} from "react-router-dom";





function App() {
  return (
    <>    
      <div className="App">
        <nav className="navbar">
          <div className="logo_container"> 
            <a href="/inicio"> 
              <img src={logo} className="logo" alt="logo"/>         
            </a>
          </div>
          <div className="category_menu">
      
            <ul>            
              <li>
                Alimentos
              </li>
              <li>
                Snacks
              </li>
              <li>
                Accesorios
              </li>
              <li>
                Aseo y Belleza
              </li>
              <li>
                Bienestar
              </li>

            </ul>
          </div>
          <figure>
            <img/>
          </figure>
          <div className="secondary_menu" >
            <a href="/">Iniciar Sesión</a>
            <a href="/registro" >Registrarse</a>
            <a href="/carrito"> 
              <img src = {carshop} className="shop_car" alt="Carrito de compras" /> 
            </a>
            
          </div>
        </nav>

        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Login/>}/>
            <Route path="/inicio" element={<Inicio/>} /> 
            <Route path="/registro" element={<Registro/>} /> 
            <Route path="/carrito" element={<Carrito/>} /> 

            
          </Routes>
        </BrowserRouter>
      </div>

      <footer className="footer">
        <p className="text_footer">Hecho con &#128151;</p>
      </footer>
    </>
  );
}

export default App;
