import React from "react";
import cat from "../images/cat.jpg";
import dog from "../images/dog.jpg";
import product from "../images/concentrado.jpg"
import { Link } from "react-router-dom";
import {useState} from "react";
import "./styles.css";


const Inicio = () => {

 const [productos, setProducto] = useState([]);

  const cargarProductos = () => {
    fetch('http://localhost:3900/api/v1/productos')
      
      .then(res => res.json())      
      .then(todosProd => setProducto(todosProd.json()));
      
      
  }

  cargarProductos();

  return (
    <>     

      <section className="hero">


      </section>

      <section className="categories">
        <div className="category_button">
          <img src={cat} className="img_button dog_button" alt="Product"/>
        </div>
        <div className="category_button ">
          <img src={dog} className="img_button cat_button" alt="Product"/>
        </div>
      </section>

      <main className="products_container">

        <h1 className="product_title">Productos de Cat & Dog Shop</h1>


        <div className="article_container">
      
          {productos.map(cadaProd => { 

            <article className="card_container">
              <div className="img_container">
                <img src={cadaProd.imagenProducto} className="product_image" alt="Imagen de producto" />
              </div>

              <div className="info_container">
                
                <h2 className="product_price">
                    {cadaProd.precioProducto}
                </h2>
                
                <h2 className="product_name">
                  {cadaProd.nombreProducto} 
                </h2>
                <p className="product_category">Alimento premium especial</p>
                

              </div>

            </article>


          })}
         
          <article className="card_container">
            <div className="img_container">
              <img src={product} className="product_image" alt="Imagen de producto" />
            </div>

            <div className="info_container">
              
              <h2 className="product_price">
                  $5.000,00
              </h2>
              
              <h2 className="product_name">
                Concentrado 
              </h2>
              <p className="product_category">Alimento premium especial</p>
              

            </div>

          </article>

          <article className="card_container">
            <div className="img_container">
              <img src={product} className="product_image" alt="Imagen de producto" />
            </div>

            <div className="info_container">
              
              <h2 className="product_price">
                  $5.000,00
              </h2>
              
              <h2 className="product_name">
                Concentrado 
              </h2>
              <p className="product_category">Alimento premium especial</p>
              

            </div>

          </article>

          <article className="card_container">
            <div className="img_container">
              <img src={product} className="product_image" alt="Imagen de producto" />
            </div>

            <div className="info_container">
              
              <h2 className="product_price">
                  $5.000,00
              </h2>
              
              <h2 className="product_name">
                Concentrado 
              </h2>
              <p className="product_category">Alimento premium especial</p>
              

            </div>

          </article>
        </div>

      </main>
      
    </>
  )
};

export default Inicio;

