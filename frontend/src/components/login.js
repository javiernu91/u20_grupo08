import { Link } from "react-router-dom";
import React from "react";
import "./styles.css";


const login = () => {
  return(
    <>          
      <div class="form_container">
        <form action="form" class="form">          
          <h2 class="form_title" >Acceso a Clientes</h2>
        
          <div class="form_input form_email">
            <label for="user_email">E-mail</label>
            <input id="user_email" type="email" placeholder="ejemplo@gmail.com"/>    
          </div> 
          <div class="form_input form_pas">
            <label for="user_pas">Contraseña</label>
            <input id="user_pas" type="password" placeholder="Contraseña"/>    
          </div> 
          
          <button class="button" type="submit">Ingresar</button>
          <Link to="/inicio" className="register">Registrarse</Link>

        </form>
        {/* <div class="button index_button">
          <a  href="./inicio">Inicio</a>
        </div> */}
      </div>
    </>
  )
}

export default login;