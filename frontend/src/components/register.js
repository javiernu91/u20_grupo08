import { Link } from "react-router-dom";
import React from "react";
import "./styles.css";


const login = () => {
  return(
    <>          
      <div class="form_container">
        <form action="form" class="form">          
          <h2 class="form_title" >Registro Usuario</h2>

          <div class="form_input form_name">
            <label for="user_name">Nombre</label>
            <input id="user_name" type="text" placeholder="Nombre Usuario"/>    
          </div> 

          <div class="form_input form_last_name">
            <label for="user_last_name">Apellidos</label>
            <input id="user_last_name" type="text" placeholder="Apellido Usuario"/>    
          </div> 

          <div class="form_input form_cel">
            <label for="user_cel">Apellidos</label>
            <input id="user_cel" type="number" placeholder="Numero de teléfono"/>    
          </div> 
        
          <div class="form_input form_email">
            <label for="user_email">E-mail</label>
            <input id="user_email" type="email" placeholder="ejemplo@gmail.com"/>    
          </div> 
          <div class="form_input form_pas">
            <label for="user_pas">Contraseña</label>
            <input id="user_pas" type="password" placeholder="Contraseña"/>    
          </div> 
          
          <button class="button" type="submit">Registrarse</button>
          

        </form>
        {/* <div class="button index_button">
          <a  href="./inicio">Inicio</a>
        </div> */}
      </div>
    </>
  )
}

export default login;