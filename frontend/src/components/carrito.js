import React from "react";
import "./styles.css";
import product from "../images/concentrado.jpg";


const Carrito = () => {

  return(
    <>
      <section className="car_container">
        <h1 className="title_products">Carrito de productos</h1>

        <div class="card_carshop">

          <div className="image_container__car">
            <img src={product} className="product_image__car" alt="impagen el producto"/>

          </div>
          <div className="product_info__car">
            <h2 className="title_product__car">Nombre Producto</h2>
            <ul className="product_menu__car">
              <li>Eliminar</li>
              <li>Más productos</li>
              <li>Comprar ahora</li>
            </ul>

          </div>
          

          <div className="product_cant__car">
            <div className="minus_cant_product">
              <p> - </p>
            </div>
            <div className="cant_product">
              <p>1</p>
            </div>
            <div className="pluss_cant_product">
              <p>+</p>
            </div>
          </div>

          <div className="price_container__car">
            <p className="price__car">
              $ 500.000,00
            </p>
          </div>

        </div>

        <div class="card_carshop">

          <div className="image_container__car">
            <img src={product} className="product_image__car" alt="impagen el producto"/>

          </div>
          <div className="product_info__car">
            <h2 className="title_product__car">Nombre Producto</h2>
            <ul className="product_menu__car">
              <li>Eliminar</li>
              <li>Más productos</li>
              <li>Comprar ahora</li>
            </ul>

          </div>
          

          <div className="product_cant__car">
            <div className="minus_cant_product">
              <p> - </p>
            </div>
            <div className="cant_product">
              <p>1</p>
            </div>
            <div className="pluss_cant_product">
              <p>+</p>
            </div>
          </div>

          <div className="price_container__car">
            <p className="price__car">
              $ 50.000,00
            </p>
          </div>

        </div>

        <div class="card_carshop">

          <div className="image_container__car">
            <img src={product} className="product_image__car" alt="impagen el producto"/>

          </div>
          <div className="product_info__car">
            <h2 className="title_product__car">Nombre Producto</h2>
            <ul className="product_menu__car">
              <li>Eliminar</li>
              <li>Más productos</li>
              <li>Comprar ahora</li>
            </ul>

          </div>
          

          <div className="product_cant__car">
            <div className="minus_cant_product">
              <p> - </p>
            </div>
            <div className="cant_product">
              <p>1</p>
            </div>
            <div className="pluss_cant_product">
              <p>+</p>
            </div>
          </div>

          <div className="price_container__car">
            <p className="price__car">
              $ 120.000,00
            </p>
          </div>

        </div>

        <button class="button buy_button" type="submit">Continuar compra</button>

      </section>



    </>

  )

}

export default Carrito;