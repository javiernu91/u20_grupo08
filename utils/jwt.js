const jwt = require("jsonwebtoken");
const { JWT_SECRET_KEY } = require("../constants");

//generar el token
// la función del accestoken permitir al usuario de acceder a sitios autenticados de muestra página web
function createAccessToken(usuario) {
  const expToken = new Date();
  expToken.setHours(expToken.getHours() + 3);

  // datos que van dentro del objeto cargaUtil
  const cargaUtil = {
    tipo_token: "access",
    id_usuario: usuario._id,
    fecha_creacion_token: Date.now(),
    expiracion_token: expToken.getTime(),
  };

  return jwt.sign(cargaUtil, JWT_SECRET_KEY);
}
//Tiene la función de refrescar el accestoken cuando este ha expirado
function createRefreshToken(usuario) {
  const expToken = new Date();
  expToken.getMonth(expToken.getMonth() + 1);

  const cargaUtil = {
    tipo_token: "refresh",
    id_usuario: usuario._id,
    fecha_creacion_token: Date.now(),
    expiracion_token: expToken.getTime(),
  };

  return jwt.sign(cargaUtil, JWT_SECRET_KEY);
}

function decoded(token) {
  return jwt.decode(token, JWT_SECRET_KEY, true);
}

module.exports = {
  createAccessToken,
  createRefreshToken,
  decoded,
};
