const bcrypt = require("bcryptjs");
const jwt = require("../utils/jwt");
const Usuario = require("../models/usuario");

function registrarUsuario(req, res) {
  const { nombres, apellidos, direccion, telefono, email, password } = req.body; //variables que reciben la información del form

  if (!email) res.status(400).send({ msg: "El email es obligatorio" });
  if (!password) res.status(400).send({ msg: "La contraseña es obligatoria" });

  //se registra el usuario
  const usuario = new Usuario({
    nombres,
    apellidos,
    direccion,
    telefono,
    email: email.toLowerCase(), //tolowercase siempre se registre en minuscula
    role: "user",
    active: false,
  });
  //encriptación de contraseña
  const salt = bcrypt.genSaltSync(10);
  const hashPassword = bcrypt.hashSync(password, salt);
  usuario.password = hashPassword;

  //Guardar el usuario en la base de datos
  usuario.save((error, usuarioAlmacenado) => {
    if (error) {
      res.status(400).send({ msg: "Error al crear el usuario" });
    } else {
      res.status(200).send(usuarioAlmacenado);
    }
  });
}
//Función de logueo
function login(req, res) {
  const { email, password } = req.body; //recibe email y password

  if (!email) res.status(400).send({ msg: "El email es obligatorio" });
  if (!password) res.status(400).send({ msg: "La contraseña es obligatoria" });

  const minusculaEmail = email.toLowerCase(); //para convertir el email en miniscula

  //busca un unico email
  Usuario.findOne({ email: minusculaEmail }, (error, usuarioAlmacenado) => {
    if (error) {
      res.status(500).send({ msg: "Error del servidor" });
    } else {
      //compara una contraseña encriptada con una normal
      bcrypt.compare(
        password,
        usuarioAlmacenado.password,
        (bcryptError, check) => {
          if (bcryptError) {
            res.status(500).send({ msg: "Error del servidor" });
          } else if (!check) {
            // si check es incorrecta
            res.status(400).send({ msg: "Contraseña incorrecta" });
          } else if (!usuarioAlmacenado.active) {
            res.status(401).send({ msg: "Usuario no autorizado o no activo" });
          } else {
            res.status(200).send({
              // el access y el refresh viene de las función del utils/jwt.js
              access: jwt.createAccessToken(usuarioAlmacenado),
              refresh: jwt.createRefreshToken(usuarioAlmacenado),
            });
          }
        }
      );
    }
  });
}
function refreshAccessToken(req, res) {
  const { token } = req.body;

  if (!token) res.status(400).send({ msg: "Token requerido" });

  const { user_id } = jwt.decoded(token);

  Usuario.findOne({ usuario_id: user_id }, (error, usuarioAlmacenado) => {
    if (error) {
      res.status(500).send({ msg: "Error del servidor" });
    } else {
      res.status(200).send({
        accessToken: jwt.createAccessToken(usuarioAlmacenado),
      });
    }
  });
}

module.exports = {
  registrarUsuario,
  login,
  refreshAccessToken,
};
