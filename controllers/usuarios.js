const bcrypt = require("bcryptjs");
const User = require("../models/usuario");
const image = require("../utils/image");

//devuelve los datos del usuario que esta logueado
async function usuarioLogueado(req, res) {
  const { id_usuario } = req.user; //este id_usuario es del jwt en carga util

  const response = await User.findById(id_usuario);

  if (!response) {
    res.status(400).send({ msg: "No se ha encontrado usuario" });
  } else {
    res.status(200).send(response);
  }
}

//obtener todos los usuarios, los activos y los inactivos
async function obtenerTodosUsuarios(req, res) {
  const { active } = req.query;
  let response = null;

  if (active === undefined) {
    response = await User.find(); //devuekve todos los usuarios
  } else {
    response = await User.find({ active }); // devuelve los activos y inactivos segun el active=true o false
  }

  res.status(200).send(response);
}

//Crear usuarios
async function crearUsuario(req, res) {
  //Lo primero es encriptar la contraseña
  const { password } = req.body;
  const nuevo_user = new User({ ...req.body, active: false });

  const salt = bcrypt.genSaltSync(10);
  const hasPassword = bcrypt.hashSync(password, salt);
  nuevo_user.password = hasPassword;

  //el usuario se puede crear con imagen o sin imagen
  if (req.files.foto) {
    const imagePath = image.getFilePath(req.files.foto);
    nuevo_user.foto = imagePath;
  }
  //guardar el usuario
  nuevo_user.save((error, userStored) => {
    if (error) {
      res.status(400).send({ msg: "Error al crear el usuario" });
    } else {
      res.status(201).send(userStored);
    }
  });
}
//actualizar usuario
//actualizar usuario
async function actualizarUsuario(req, res) {
  const { id_usuario } = req.params;
  const userData = req.body; //datos que me llegan desde el cliente

  if (userData.password) {
    const salt = bcrypt.genSaltSync(10);
    const hashPassword = bcrypt.hashSync(userData.password, salt);
    userData.password = hashPassword;
  } else {
    delete userData.password;
  }

  if (req.files.foto) {
    const imagePath = image.getFilePath(req.files.foto);
    userData.foto = imagePath;
  }

  User.findByIdAndUpdate({ _id: id_usuario }, userData, (error) => {
    if (error) {
      res.status(400).send({ msg: "Error al actualizar el usuario" });
    } else {
      res.status(200).send({ msg: "Actualizacion correcta" });
    }
  });
}

//Eliminar un usuario
async function eliminarUsuario(req, res) {
  const { id } = req.params;

  User.findByIdAndDelete(id, (error) => {
    if (error) {
      res.status(400).send({ msg: "Error al eliminar el usuario" });
    } else {
      res.status(200).send({ msg: "Usuario eliminado" });
    }
  });
}
module.exports = {
  usuarioLogueado,
  obtenerTodosUsuarios,
  crearUsuario,
  actualizarUsuario,
  eliminarUsuario,
};
