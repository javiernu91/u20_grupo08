const Venta = require('../models/venta');
const image = require('../utils/image');

//Crear la venta
function crearVenta(req, res) {
  const nueva_venta = new Venta(req.body);

  //recibe imagenes en la constante imagepath
  const imagePath = image.getFilePath(req.files.imagen);
  nueva_venta.imagen = imagePath;

  //guardar la venta
  nueva_venta.save((error, ventaAlmacenada) => {
    if (error) {
      res.status(400).send({ msg: 'Error al realizar la venta' });
    } else {
      res.status(201).send(ventaAlmacenada);
    }
  });
}

module.exports = {
  crearVenta,
};
