const express = require('express');
const multiparty = require('connect-multiparty');
const UserController = require('../controllers/venta');
const md_auth = require('../middlewares/authenticated');

const md_upload = multiparty({ uploadDir: './uploads/imageProduct' }); //guardar imagenes a esa ruta
const api = express.Router();

api.post('/ventas/create', UserController.crearVenta);

module.exports = api;
