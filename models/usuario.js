const mongoose = require("mongoose");

const EsquemaUsuario = mongoose.Schema({
  nombres: String,
  apellidos: String,
  direccion: String,
  telefono: String,
  email: {
    type: String,
    unique: true,
  },
  password: String,
  role: String,
  active: Boolean,
  foto: String,
});

module.exports = mongoose.model("Usuario", EsquemaUsuario);
